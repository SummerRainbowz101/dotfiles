## qutebrowser

I use qutebrowser for the vim keybindings, flexibility, and attentive dev. For some reason, Firefox runs slow/lags after about an hour of use on my system, so this or chromium is what I use. When using chromium, I configure it to conform to the GTK settings. I have a qutebrowser template file which gets edited to conform colors to the current terminal color scheme.
