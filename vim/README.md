## vim 

I use vim plug to manage my plugins, for its lazy loading based on filetype options, toggleable actions, and minimal syntax.

My vimrc utilizes vim folds, with the following sections:
- Plugins
- Plugin settings
- Misc

plugin | notes
----|-----
[tpope/vim-sensible](https://github.com/tpope/vim-sensible) |  sensible defaults. 
[bling/vim-airline](https://github.com/bling/vim-airline) |  Status line 
[jeffkreeftmeijer/vim-numbertoggle](https://github.com/jeffkreeftmeijer/vim-numbertoggle) |  Auto relative number toggling 
[airblade/vim-gitgutter](https://github.com/airblade/vim-gitgutter) |  Live git changes visible 
[tpope/vim-fugitive](https://github.com/tpope/vim-fugitive) |  Complement git in vim - todo: learn this. 
[tpope/vim-sleuth](https://github.com/tpope/vim-sleuth) |  Auto spacing/indenting conformity to files 
[terryma/vim-multiple-cursors](https://github.com/terryma/vim-multiple-cursors) |  Muliple cursors, akin to sublime text 
[jiangmiao/auto-pairs](https://github.com/jiangmiao/auto-pairs) |  auto-pairs(brackets/quotes) 
[scrooloose/nerdtree](https://github.com/scrooloose/nerdtree) |  Side panel file browser. 
[Valloric/YouCompleteMe](https://github.com/Valloric/YouCompleteMe) |  Autocompletion engine. 
[rdnetto/YCM-Generator](https://github.com/rdnetto/YCM-Generator) |  Generate ycm files - :YcmGenerateConfig 
[mattn/emmet-vim](https://github.com/mattn/emmet-vim) |  A tool for generating repetitive html/css. todo: learn this. 
[Valloric/MatchTagAlways](https://github.com/Valloric/MatchTagAlways) |  Autocompletes tags. 
[junegunn/fzf](https://github.com/junegunn/fzf) |  based fuzzy search. 
