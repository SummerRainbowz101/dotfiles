#
# bspwm hotkeys
#

# note a desktop
super + c
    echo "$(bspc query -D -d)" > /tmp/bspwmdtop_swap

# move the windows of the noted desktop to the current desktop
super + v
    for win in $(bspc query -N -d $(cat /tmp/bspwmdtop_swap)); do \
        bspc node $win -d $(bspc query -D -d); \
    done

super + w
    bspc node -c

super + t
    bspc desktop -l next && bspc node -f next

super + b
    bspc desktop -B

super + s
    bspc query -N -n focused.tiled && state=floating || state=tiled; \
    bspc node -t $state

super + f
    state=fullscreen; \
    bspc query -N -n "focused.$state" && state=$(bspc query -T -n | jshon -e client -e lastState -u); \
    bspc node -t "$state"

super + {_,shift} + Tab
    bspc desktop -f {next,prev}

alt + {_,shift} + Tab
    bspc node -f {next,prev}

super + apostrophe
    bspc node -s last

super + m
    bspc node -s biggest

super + shift + {h,j,k,l}
    bspc config pointer_follows_focus true; \
    cur_win=$(bspc query -N -n); \
    cur_mon=$(bspc query -M -m); \
    dir={west,south,north,east}; \
    if ! bspc node -f $dir; then \
        bspc node -m $dir; \
        bspc monitor -f $dir; \
    else \
        new_mon=$(bspc query -M -m); \
        if [ $new_mon -eq $cur_mon ]; then \
            bspc node -s $cur_win; \
        else \
            bspc node $cur_win -m ^$new_mon; \
        fi; \
        bspc node -f $cur_win; \
    fi; \
    bspc config pointer_follows_focus false

super +  {h,j,k,l}
    bspc config pointer_follows_monitor true; \
    bspc config pointer_follows_focus true; \
    dir={west,south,north,east}; \
    if ! bspc node -f $dir; then \
        bspc monitor -f $dir; \
    fi; \
    bspc config pointer_follows_monitor false; \
    bspc config pointer_follows_focus false

super + {comma,period}
    bspc desktop -C {backward,forward}

super + ctrl + {h,j,k,l}
    bspc node -p {west,south,north,east}

super + ctrl + {_,shift + }space
    bspc node @{_,/} -p cancel

# resize functions - todo: have these do things to floating windows.
super + alt + {h,j,k,l}
    bspc node {@west -r -35,@south -r +35,@north -r -35,@east -r +35}

super + alt + shift + {h,j,k,l}
    bspc node {@east -r -35,@north -r +35,@south -r -35,@west -r +35}

super + ctrl + {1-9}
    bspc node -r 0.{1-9}

# These two keybinds determine index by list order of desktops from current monitor
super + {1-9}
    D={1-9}; \
    bspc desktop -f "$(bspc query -D -m | sed -n "$D p")"

super + shift + {1-9}
    D={1-9}; \
    bspc node -d "$(bspc query -D -m | sed -n "$D p")"

~button1
    bspc pointer -g focus

# mouse movements
super + button{1-3}
    bspc pointer -g {move,resize_side,resize_corner}

super + !button{1-3}
    bspc pointer -t %i %i

super + @button{1-3}
    bspc pointer -u

# kill panel, nicely ask all windows to close, quit.
# todo: ask nicely for real.
super + shift + q
    pgrep lemonbar && ~/.wm/scripts/panelt; \
    for win in $(bspc query -N); do bspc node $win -c ; done; \
    bspc quit

# rotate desktops.
super + {_,shift} + semicolon
    bspc node @/ -R {90,270}

# toggle panel or gaps.
super + {_,shift} + slash
    ~/.wm/scripts/{gapt,panelt}

#
# wm independent hotkeys
#
super + p
    ~/.wm/scripts/i3blur.sh

super + o
    $BROWSER

super + e
    pcmanfm

super + Return
    $TERMINAL

super + space
    eval dmenu_run $(dmenu_options)

super + shift + space
    eval passmenu $(dmenu_options)

XF86Audio{Prev,Next}
     mpc -q {prev,next}


XF86AudioPlay
     mpc -q toggle

XF86Audio{LowerVolume,RaiseVolume}
    amixer -q sset Master 3%{-,+}

XF86AudioMute
    amixer -q set Master toggle

# make sxhkd reload its configuration files:
super + Escape
    pkill -USR1 -x sxhkd
